import React, { useState, useEffect } from "react";
import InstantiateDialog from "./instantiate/InstantiateDialog";

import { Grid } from "@mui/material";
import { refreshAlliances } from "./utils/DropDownManager";
import UserSelection from "./user_selection/UserSelection";

function SetupComponents({bpmId}){
    const[alliances, setAlliances] = React.useState([]);

    React.useEffect(() => {
        refreshAlliances(document.bpmId, setAlliances);
        document.bpmId = "";}, [])

    return(<>
    <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
        <Grid item>
            Current User:
        </Grid>
        <Grid item>
            <UserSelection setAlliances={setAlliances}/>
        </Grid>
        <Grid item>
            <InstantiateDialog alliances={alliances}/>
        </Grid>
    </Grid>
    </>)
}

export default SetupComponents;