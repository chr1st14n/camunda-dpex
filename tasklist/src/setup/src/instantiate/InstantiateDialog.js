import React, { useState, useEffect } from "react";
import { Dialog, Button, DialogContent, DialogTitle, Grid, NativeSelect} from "@mui/material";
import styled from 'styled-components';
import Utility from "../utils/Utility";
import Variables from "../utils/Variables";

function InstantiateDialog({alliances}){
    const [open, setOpen] = React.useState(false);
    const [error_status, setErrorStatus] = React.useState("");
    const [variables, setVariables] = React.useState({});
    const [gar, setGar] = React.useState("");

    const handleClickToOpen = () => {
        setOpen(true);
    };
 
    const handleToClose = () => {
        setOpen(false);
    };


    return(<>
    <Button variant="outlined" onClick={handleClickToOpen} sx={{ p: 2 }} style={{
          maxWidth: "5em",
          maxHeight: "3em"
        }}>dpex-instantiate</Button>
        <Dialog open={open} onClose={handleToClose}>
        <DialogTitle>
            <h4>Instantiate a process</h4>
        </DialogTitle>
        <DialogContent>
            <Grid container direction="row" spacing={1} padding={1}>
                <Grid>
                    <Grid container direction="column" spacing={2} paddingTop={1.5}>
                        <Grid container direction="row" spacing={1} padding={1}>
                            <Grid item xs={4}>
                                Name: 
                            </Grid>
                            <Grid item xs={8}>
                                <input id="name" />
                            </Grid>
                        </Grid>
                        <Grid container direction="row" spacing={1} padding={1}>
                            <Grid item xs={4}>
                                Alliance: 
                            </Grid>
                            <Grid item xs={8}>
                                <NativeSelect style={{minWidth:"80%"}} onChange={(event) => handleAllianceSelection(event, setGar)}>
                                    {Utility.createKnownAllianceElements(alliances)}
                                </NativeSelect>
                            </Grid>
                        </Grid>
                        <Grid container direction="row" spacing={1} padding={1}>
                            <Grid item xs={4}>
                                Process Variables:<br></br>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Variables variables={variables} setVariables={setVariables}/>
                    <ButtonGroup style={{marginTop:"10px"}}>
                        <Button variant="outlined" onClick={() => instantiate_dpex(variables, setErrorStatus, gar)} style={{padding:"8px"}}>
                            Instantiate
                        </Button>   
                    </ButtonGroup>
                    <span>{error_status}</span>
                </Grid>
            </Grid>
            <span id="instantiate_msg"></span>
        </DialogContent>
        </Dialog>
    </>
    );
}


export default InstantiateDialog;

function instantiate_dpex(variables, setErrorStatus, gar){

    setErrorStatus("Loading...")

    var bpm_id = document.bpmId;
    var name = document.getElementById("name").value;

    const url = "http://localhost:8086/api/instantiate";
    
    const request = new XMLHttpRequest();
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                setErrorStatus("Success!");
            }else{
                setErrorStatus("Error: " + request.status);
            }
        }
    }
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");

    var body = {
        'gar': gar,
        'bpmId': bpm_id,
        'name': name,
        'variables': JSON.stringify(variables)
    }

    request.send(JSON.stringify(body));
}

const ButtonGroup = styled.div`
  display: flex;
`

function handleAllianceSelection(event, setGar){
    setGar(event.target.value)
}