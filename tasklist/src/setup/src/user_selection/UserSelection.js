import React from "react";
import Utility from "../utils/Utility";
import {NativeSelect} from "@mui/material";
import { refreshAlliances } from "../utils/DropDownManager";

const UserSelection = ({setAlliances}) => {
    const [users, setUsers] = React.useState([]);

    React.useEffect(() => {
        Utility.refreshUsers(setUsers);
    }, [])

    return(<>
        <NativeSelect id="user_selection" style={{minWidth:"80%"}} onChange={(event) => handleUserSelection(event, setAlliances)}>
            {Utility.createUserElements(users)}
        </NativeSelect>
    </>)
}

const handleUserSelection = (event, setAlliances) => {
    document.bpmId = event.target.value;
    refreshAlliances(document.bpmId, setAlliances);
}

export default UserSelection;