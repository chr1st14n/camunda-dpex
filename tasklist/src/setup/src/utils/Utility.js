import React, { useState, useEffect } from "react";

const createKnownAllianceElements = (alliances) => {
    let alliance_items = [];
    alliance_items.push(<option value={""}>{"--"}</option>);  
    for(var a of alliances){ 
        alliance_items.push(<option value={a["gar"]}>{a["name"]}</option>);  
    }    
    return alliance_items;
}

const createUserElements = (users) => {
    let alliance_items = [];
    alliance_items.push(<option value={""}>{"--"}</option>);  
    for(var u of users){   
        alliance_items.push(<option value={u["bpmId"]}>{u["bpmId"]}</option>);  
    }    
    return alliance_items;
}

const refreshUsers = (setUsers) => {
    const url = "http://localhost:8086/api/allUsers";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if (request.status > 199 && request.status < 300) {
                setUsers(JSON.parse(request.responseText));
            } 
        }
    };
    request.send();
}

export default {createKnownAllianceElements, createUserElements, refreshUsers};