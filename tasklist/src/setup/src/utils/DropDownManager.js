var base_url = "http://localhost:8086/api/"

function refreshAlliances(bpmId, setAlliances){
    const url = base_url + "alliances?bpmId=" + bpmId;
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var alliances = JSON.parse(request.responseText);
                setAlliances(alliances);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

function refreshAllAdapters(){
    refreshBpmAdapters();
    refreshCollAdapters();
    refreshCommAdapters();
}

function refreshBpmAdapters(){
    const url = base_url + "bpmEngines";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                var names = [];
                for(var a of adapters){
                    names.push(a["name"])
                }
                window.dpex_bpm_adapters = adapters;
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

function refreshCollAdapters(){
    const url = base_url + "collaborations";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                var names = [];
                for(var a of adapters){
                    names.push(a["name"])
                }
                window.dpex_coll_adapters = adapters;
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

function refreshCommAdapters(){
    const url = base_url + "communicationModules";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                var names = [];
                for(var a of adapters){
                    names.push(a["name"])
                }
                window.dpex_comm_adapters = adapters;
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

export {refreshAlliances, refreshAllAdapters};