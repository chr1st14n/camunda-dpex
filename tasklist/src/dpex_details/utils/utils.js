import React, { useState, useEffect } from "react";

const createKnownGarElements = function (gars){
    let gar_items = [];
    gar_items.push(<option value={""}>{"--"}</option>);  
    for(var g of gars){   
        gar_items.push(<option value={g}>{g}</option>);  
    }    
    return gar_items;
}

export {createKnownGarElements};