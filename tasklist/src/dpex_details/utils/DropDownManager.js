function refreshAlliances(bpmId){
    const url = "http://localhost:8086/api/alliances?bpmId=" + bpmId;
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var alliances = JSON.parse(request.responseText);
                var gars = [];
                for(var a of alliances){
                    gars.push(a["gar"])
                }
                window.dpex_alliances = alliances;
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

export {refreshAlliances};