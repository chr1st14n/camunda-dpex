import React, { useState } from "react";

import { Button, Grid } from "@mui/material";

function ClaimButton() {
  const [error_status, setErrorStatus] = useState(""); 

  return (<>
    <Button variant="outlined" onClick={() => claim(setErrorStatus)}>Claim</Button><br></br>
    <span>{error_status}</span>
  </>);
}

export default ClaimButton;

async function claim(setErrorStatus){
    var url = "http://localhost:8086/api/camundaClaim";
    const heads = {
      "Content-type": "application/json"
    }
    
    var body = {
      "lir": window.my_camunda_task["task"]["processInstanceId"],
      //"task": window.my_camunda_task["task"]["name"],
      "task": window.my_camunda_task["task"]["name"],
      "taskLifeCycleStage": "CLAIM",
      "bpmId": getBpmId(),
      "processVariables": ""
    };
    
    const request = new XMLHttpRequest();

    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                setErrorStatus("Success!");
            }else{
              setErrorStatus("Error: " + request.status);
            }
        }
    };
    setErrorStatus("Loading...")
    request.send(JSON.stringify(body));
  }

  function getBpmId(){
    return document.bpmId;
  }
