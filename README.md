<h1>Setup</h1>
To setup the dpex cockpit and tasklist plugin you need to execute the setup.sh script while specifing the camunda directory as cmd line argument.

<h2>Example:</h2>
This will build from source, configure camunda and move the necessary files: <br>
<code>
./setup.sh /home/bastian/camunda-bpm-tomcat-7.19.0/server/apache-tomcat-9.0.72
</code>
<br>
<p>
This will always build all the projects and then configure camunda and move the necessary files. If you haven't changed any code you can just execute setup_simple.sh
</p>

<h2>Example:</h2>
This will just configure camunda and move the necessary files: <br>
<code>
./setup_simple.sh /home/bastian/camunda-bpm-tomcat-7.19.0/server/apache-tomcat-9.0.72
</code>
<br>
<p>
This will just place the existing files into the correct locations so it won't compile or look at the source code in any way
</p>

# Docker Integration
To build and run the docker image execute the following commands.

 - ``docker build . -t dpex-camunda-plugin``
 - ``docker run -p 8080:8080 dpex-camunda-plugin``
