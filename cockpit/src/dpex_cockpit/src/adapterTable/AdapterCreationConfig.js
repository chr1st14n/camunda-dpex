import * as React from 'react';
import { NativeSelect, Grid } from '@mui/material';

function AdapterCreationConfig({handleSelection, setChosenAdapter, adapterTypes}){
    return(<>
        <Grid container direction="column" spacing={2} paddingTop={1.5} minWidth={"100%"} maxWidth={"100%"}>
            <Grid container direction="row" spacing={1} padding={1}>
                <Grid item xs={4}>
                    Type:
                </Grid>
                <Grid item xs={8}>
                    {createAdapterSelection(handleSelection, setChosenAdapter, adapterTypes)}
                </Grid>
            </Grid>
            <Grid container direction="row" spacing={1} padding={1} minWidth={"100%"} maxWidth={"100%"}>
                <Grid item xs={4}>
                    Name:
                </Grid>
                <Grid item xs={8}>
                <input id={"adapter_creation_name"} name="name" style={{paddingInline: "1%"}} minWidth={"80%"} maxWidth={"80%"}/>
                </Grid>
            </Grid>
            <Grid container direction="row" spacing={1} padding={1} minWidth={"100%"} maxWidth={"100%"}>
                <Grid item xs={4}>
                    URL:
                </Grid>
                <Grid item xs={8}>
                <input id={"adapter_creation_connection"} name="connection" style={{paddingInline: "1%"}} minWidth={"80%"} maxWidth={"80%"}/>
                </Grid>
            </Grid>
        </Grid>
    </>)
}


const createAdapterSelection = function (handle, setter, adapterTypes){
  let selection = [];
  selection.push(
      <NativeSelect style={{minWidth:"100%"}} onChange={(event) => handle(event, setter)}>
          {createAdapterElements(adapterTypes)}
      </NativeSelect>
  )
  return selection;
}

const createAdapterElements = function (adapterTypes){
  let adapter_items = [];
  adapter_items.push(<option value={""}>{"--"}</option>);  
  for(var a of adapterTypes){
      adapter_items.push(<option value={a.value}>{a.label}</option>);  
  }    
  return adapter_items;
}

export default AdapterCreationConfig;