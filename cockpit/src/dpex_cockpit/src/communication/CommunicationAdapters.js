import React from "react";
import CommunicationAdapterButtons from "./CommunicationAdapterButtons";
import AdapterTable from "../adapterTable/AdapterTable";

function CommunicationAdapters({commAdapters, setCommAdapters}){
    return(<>
        <h3>Communication Adapters</h3>
        <AdapterTable  attributes={["Name", "Type", "URL"]} api_attributes={["name", "type", "connection"]} adapters={commAdapters}/>
        <CommunicationAdapterButtons setAdapters={setCommAdapters} adapters={commAdapters}/>
    </>
    );
}

export default CommunicationAdapters;
