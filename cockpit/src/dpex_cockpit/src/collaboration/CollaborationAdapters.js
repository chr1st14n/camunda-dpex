import React from "react";
import AdapterTable from "../adapterTable/AdapterTable";
import CollaborationAdapterButtons from "./CollaborationAdapterButtons";

function CollaborationAdapters({collAdapters, setCollAdapters}){  
    return(<>
        <h3>Collaboration Adapters</h3>
        <AdapterTable attributes={["Name", "Type", "URL"]} api_attributes={["name", "type", "connection"]} adapters={collAdapters}/>
        <CollaborationAdapterButtons setAdapters={setCollAdapters} adapters={collAdapters}/>
    </>
    );
}

export default CollaborationAdapters;
