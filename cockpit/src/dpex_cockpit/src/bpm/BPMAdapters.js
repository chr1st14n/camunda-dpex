import React from "react";
import AdapterTable from "../adapterTable/AdapterTable";
import BPMAdapterButtons from "./BPMAdapterButtons";

function BPMAdapters({bpmAdapters, setBpmAdapters}){
    return(<>
        <h3>BPM Adapters</h3>
        <AdapterTable attributes={["Name", "Type", "URL"]} api_attributes={["name", "type", "connection"]} adapters={bpmAdapters}/>
        <BPMAdapterButtons setAdapters={setBpmAdapters} adapters={bpmAdapters}/>
    </>
    );
}

export default BPMAdapters;
