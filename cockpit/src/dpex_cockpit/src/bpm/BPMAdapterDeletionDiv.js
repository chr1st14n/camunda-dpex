import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import { Button, Input, Grid } from '@mui/material';
import ButtonGroup from '../ButtonGroup';
import Utility from '../utils/Utility';

const BPMAdapterDeletionDiv = ({closeDialog, setAdapters, adapters}) => {
  const [checked, setChecked] = React.useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  if(adapters.length > 0){
    return (<>
      <List dense sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {adapters.map((value) => {
          const labelId = `${value.name}`;
          return (
            <ListItem
              key={value}
              secondaryAction={
                <Checkbox
                  edge="end"
                  onChange={handleToggle(value)}
                  checked={checked.indexOf(value) !== -1}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              }
              disablePadding
            >
              <ListItemButton>
                <ListItemText id={labelId} primary={value.name} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <ButtonGroup>
        <Button variant="outlined" onClick={() => deleteSelected(checked, setAdapters)}>
            Delete Selected
        </Button>      
        <span id="deploy_msg"></span>
      </ButtonGroup>
      </>
    );
  }else{
    return (<>
      No adapters to delete
      <ButtonGroup>
        <Button variant="outlined" onClick={() => deleteSelected(checked, setAdapters)}>
            Delete Selected
        </Button>      
        <span id="deploy_msg"></span>
      </ButtonGroup>
      </>
    );
  }
}

function deleteSelected(checked, setAdapters){
    for(var a of checked){
      const url = "http://localhost:8086/api/bpmEngine?name=" + a.name;
        const request = new XMLHttpRequest();
    
        request.open("DELETE", url, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onreadystatechange = () => {
            if(request.readyState == 4){
                Utility.refreshBpmAdapters(setAdapters);
            }
        };
        request.send();
    }
}

export default BPMAdapterDeletionDiv;