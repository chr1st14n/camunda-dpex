import React, { useState } from "react";
import { Dialog, Button, DialogContent, DialogTitle, Grid} from "@mui/material";
import ButtonGroup from "../ButtonGroup";
import Utility from "../utils/Utility";
import AllianceDeletionDiv from "./AllianceDeletionDiv";

const AllianceButtons = ({bpmId, alliances, setAlliances}) => {
    const [open, setOpen] = React.useState(false);

    const handleClickToOpen = () => {
        setOpen(true);
    };
 
    const handleToClose = () => {
        setOpen(false);
    };

    return (<>
    <Button variant="outlined" onClick={handleClickToOpen} sx={{ p: 2 }} style={{
            maxWidth: "5em",
            maxHeight: "3em",
            paddingTop: "2em",
            paddingLeft: "2em"
        }}>Delete Alliance</Button>
        <Dialog open={open} onClose={handleToClose} style={{minWidth: 2}}>
            <DialogTitle><h4>Delete alliances</h4></DialogTitle>
            <DialogContent>
                <AllianceDeletionDiv bpmId={bpmId} setAlliances={setAlliances} alliances={alliances}/>  
            </DialogContent>
        </Dialog>
    </>);
 }

 export default AllianceButtons;