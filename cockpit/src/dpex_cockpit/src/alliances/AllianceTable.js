import React, { useState, useEffect} from "react";
import {Table, TableContainer, TableBody, TableHead, TableCell, TableRow} from "@mui/material";

function AllianceTable({alliances}) {
    if(alliances.length == 0){
        return(<>
            <br></br>There are no alliances yet<br></br>
        </>
        )
    }
    return (
        <TableContainer style={{fontSize: "12px"}}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <AlliancesTableCell content={"Name"}/>
                        <AlliancesTableCell content={"GAR"}/>
                        <AlliancesTableCell content={"Process Model"}/>
                        <AlliancesTableCell content={"Instances"}/>
                        <AlliancesTableCell content={"Engine"}/>
                        <AlliancesTableCell content={"Collab."}/>
                        <AlliancesTableCell content={"Comm."}/>
                    </TableRow>
                </TableHead>
                {CreateAllianceTableRows(alliances)}
            </Table>
        </TableContainer>
    )
}

const CreateAllianceTableRows = function (alliances){
    var alliance_rows = [];
    for(var i = 0; i < alliances.length; i++){   
        alliance_rows.push(
            <TableRow style={{fontSize: "12px"}}>
                <AlliancesTableCell content={alliances[i]["name"]}/>
                <AlliancesTableCell content={alliances[i]["gar"]}/>
                <AlliancesTableCell content={alliances[i]["processModel"]}/>
                <AlliancesTableCell content={alliances[i]["instances"].length}/>
                <AlliancesTableCell content={alliances[i]["bpmEngine"]}/>
                <AlliancesTableCell content={alliances[i]["collaboration"]}/>
                <AlliancesTableCell content={alliances[i]["communicationModule"]}/>
            </TableRow>);  
    }    
    return alliance_rows;
}

function AlliancesTableCell ({alignment = "left", content}) {
    return(<>
        <TableCell align={alignment} style={{fontSize:"12px"}}>{content}</TableCell>
    </>)
}

export default AllianceTable;
