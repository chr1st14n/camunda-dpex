import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import { Button, Input, Grid } from '@mui/material';
import ButtonGroup from '../ButtonGroup';
import Utility from '../utils/Utility';

const AllianceDeletionDiv = ({bpmId, setAlliances, alliances}) => {
  const [checked, setChecked] = React.useState([]);
  const [error_status, setErrorStatus] = React.useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };
  if(alliances.length > 0){
    return (<>
      <List dense sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {alliances.map((value) => {
          const labelId = `${value.name}`;
          return (
            <ListItem
              key={value}
              secondaryAction={
                <Checkbox
                  edge="end"
                  onChange={handleToggle(value)}
                  checked={checked.indexOf(value) !== -1}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              }
              disablePadding
            >
              <ListItemButton>
                <ListItemText id={labelId} primary={value.name} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <ButtonGroup>
        <Button variant="outlined" onClick={() => deleteSelected(checked, setErrorStatus, setAlliances, bpmId)}>
            Delete Selected
        </Button>      
        <span id="deploy_msg"></span>
      </ButtonGroup>
      <span>{error_status}</span>
      </>
    );
  }else{
    return (<>
      No alliances to delete
      <ButtonGroup>
        <Button variant="outlined" onClick={() => deleteSelected(checked, setErrorStatus, setAlliances, bpmId)}>
            Delete Selected
        </Button>      
        <span id="deploy_msg"></span>
      </ButtonGroup>
      <span>{error_status}</span>
      </>
    );
  }
}

function deleteSelected(checked, setErrorStatus, setAlliances, bpmId){
    for(var a of checked){
      const url = "http://localhost:8086/api/alliance?name=" + a.name;
        const request = new XMLHttpRequest();
    
        request.open("DELETE", url, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onreadystatechange = () => {
            if(request.readyState == 4){
              if(request.status > 199 && request.status < 300){
                setErrorStatus("Success!");
                Utility.refreshAlliances(bpmId, setAlliances);
              }
            }
        };
        request.send();
    }
}

export default AllianceDeletionDiv;