import requests
import json

api_base = "http://localhost:8086/api/"

def create_comm_adapter():
    api_extension = "communicationModule"
    url = api_base + api_extension
    body = {
        "name": "py_matrix",
        "connection": "test",
        "type": "MATRIX"
    }
    result = requests.post(url=url, data=json.dumps(body), headers={"content-type":"application/json"})
    print("CommAdapter: " + str(result.status_code))

def create_coll_adapter():
    api_extension = "collaboration"
    url = api_base + api_extension
    body = {
        "name": "py_ethereum",
        "connection": "http://localhost:8545/",
        "type": "ETHEREUM_RAW"
    }
    result = requests.post(url=url, data=json.dumps(body), headers={"content-type":"application/json"})
    print("CollAdapter: " + str(result.status_code))

def create_bpm_adapter():
    api_extension = "bpmEngine"
    url = api_base + api_extension
    body = {
        "name": "py_camunda",
        "connection": "http://localhost:8080/engine-rest/",
        "type": "CAMUNDA_BPM_ENGINE"
    }
    result = requests.post(url=url, data=json.dumps(body), headers={"content-type":"application/json"})
    print("BpmAdapter: " + str(result.status_code))


create_comm_adapter()
create_coll_adapter()
create_bpm_adapter()