#check whether theres a webapps directory
if [ ! -d "$1/webapps" ]; then
  echo "Your specified camunda directory is probably not correct"  
  exit 0
fi

ROOT=${PWD}

#build all plugin parts
cd $ROOT/tasklist/src/setup && npm run build 
cd $ROOT/tasklist/src/dpex_details && npm run build 
cd $ROOT/cockpit/src/dpex_cockpit && npm run build 

#copy the plugin and config files to the necessary locations
#config files
cp $ROOT/cockpit/config.js $1/webapps/camunda/app/cockpit/scripts/config.js
cp $ROOT/tasklist/config.js $1/webapps/camunda/app/tasklist/scripts/config.js

#directories for plugin files
COCKPIT_DIRECTORY="$1/webapps/camunda/app/cockpit/scripts/ba_camunda_dpex_plugin"
TASKLIST_DIRECTORY="$1/webapps/camunda/app/tasklist/scripts/ba_camunda_dpex_plugin"
if [ ! -d "$COCKPIT_DIRECTORY" ]; then
  mkdir "$COCKPIT_DIRECTORY"
fi
if [ ! -d "$TASKLIST_DIRECTORY" ]; then
  mkdir "$TASKLIST_DIRECTORY"
fi

#copy plugin files
cp $ROOT/cockpit/src/dpex_cockpit/dist/plugin.js $COCKPIT_DIRECTORY/dpex_cockpit_plugin.js
cp $ROOT/tasklist/src/dpex_details/dist/plugin.js $TASKLIST_DIRECTORY/dpex_details_plugin.js
cp $ROOT/tasklist/src/setup/dist/plugin.js $TASKLIST_DIRECTORY/dpex_setup_plugin.js

#copy web.xml file
SETTINGS_DIRECTORY="$1/webapps/camunda/WEB-INF"
cp $ROOT/web.xml $SETTINGS_DIRECTORY/web.xml